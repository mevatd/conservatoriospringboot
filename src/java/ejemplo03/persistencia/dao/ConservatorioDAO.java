package ejemplo03.persistencia.dao;

import ejemplo03.dominio.Conservatorio;
import com.fpmislata.persistencia.dao.GenericDAO;

public interface ConservatorioDAO extends GenericDAO<Conservatorio,Integer> {

}
