package ejemplo03.persistencia.dao.impl;

import ejemplo03.persistencia.dao.CursoDAO;
import ejemplo03.dominio.Curso;
import com.fpmislata.persistencia.dao.impl.GenericDAOImplHibernate;

public class CursoDAOImplHibernate extends GenericDAOImplHibernate<Curso,Integer> implements  CursoDAO {

}
