package ejemplo03.persistencia.dao.impl;

import ejemplo03.persistencia.dao.ConservatorioDAO;
import ejemplo03.dominio.Conservatorio;
import com.fpmislata.persistencia.dao.impl.GenericDAOImplHibernate;

public class ConservatorioDAOImplHibernate extends GenericDAOImplHibernate<Conservatorio,Integer> implements  ConservatorioDAO {

}
