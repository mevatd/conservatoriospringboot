package ejemplo03.persistencia.dao;

import ejemplo03.dominio.Curso;
import com.fpmislata.persistencia.dao.GenericDAO;

public interface CursoDAO extends GenericDAO<Curso,Integer> {

   
}
