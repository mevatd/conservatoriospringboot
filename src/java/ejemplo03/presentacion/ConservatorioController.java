/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo03.presentacion;

import com.fpmislata.persistencia.dao.BussinessException;
import com.fpmislata.persistencia.dao.BussinessMessage;
import ejemplo03.dominio.Conservatorio;
import ejemplo03.dominio.Curso;
import ejemplo03.persistencia.dao.ConservatorioDAO;
import ejemplo03.persistencia.dao.CursoDAO;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Lorenzo González
 */
@Controller
public class ConservatorioController {

    @Autowired
    private ConservatorioDAO conservatorioDAO;
    @Autowired
    private CursoDAO cursoDAO;

    @RequestMapping({"/index.html"})
    public ModelAndView listarConservatorios(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> model = new HashMap<String, Object>();
        String viewName;

        try {
            List<Conservatorio> conservatorios = conservatorioDAO.findAll();
            model.put("conservatorios", conservatorios);
            viewName = "conservatorioLista";
        } catch (BussinessException ex) {
            model.put("bussinessMessages", ex.getBussinessMessages());
            model.put("backURL", request.getContextPath() + "/index.html");
            viewName = "error";
        }

        return new ModelAndView(viewName, model);
    }

    @RequestMapping({"/conservatorio/newForInsert"})
    public ModelAndView newForInsert(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> model = new HashMap<String, Object>();
        String viewName;

        try {
            Conservatorio conservatorio = conservatorioDAO.create();
            model.put("formOperation", FormOperation.Insert);
            model.put("conservatorio", conservatorio);
            viewName = "conservatorio";
        } catch (BussinessException ex) {
            model.put("bussinessMessages", ex.getBussinessMessages());
            model.put("backURL", request.getContextPath() + "/index.html");
            viewName = "error";
        }

        return new ModelAndView(viewName, model);
    }

    @RequestMapping({"/conservatorio/readForUpdate"})
    public ModelAndView readForUpdate(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> model = new HashMap<String, Object>();
        String viewName;

        try {
            int id;
            try {
                id = Integer.parseInt(request.getParameter("id"));
            } catch (NumberFormatException nfe) {
                throw new BussinessException(new BussinessMessage(null, "Se debe escribir un Id válido"));
            }

            Conservatorio conservatorio = conservatorioDAO.get(id);
            if (conservatorio == null) {
                throw new BussinessException(new BussinessMessage(null, "No existe el conservatorio con id=" + id));
            }
            model.put("formOperation", FormOperation.Update);
            model.put("conservatorio", conservatorio);
            viewName = "conservatorio";
        } catch (BussinessException ex) {
            model.put("bussinessMessages", ex.getBussinessMessages());
            model.put("backURL", request.getContextPath() + "/index.html");
            viewName = "error";
        }

        return new ModelAndView(viewName, model);
    }

    @RequestMapping({"/conservatorio/readForDelete"})
    public ModelAndView readForDelete(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> model = new HashMap<String, Object>();
        String viewName;
        try {
            int id;
            try {
                id = Integer.parseInt(request.getParameter("id"));
            } catch (NumberFormatException nfe) {
                throw new BussinessException(new BussinessMessage(null, "Se debe escribir un Id válido"));
            }

            Conservatorio conservatorio = conservatorioDAO.get(id);
            if (conservatorio == null) {
                throw new BussinessException(new BussinessMessage(null, "No existe el conservatorio con id=" + id));
            }
            model.put("formOperation", FormOperation.Delete);
            model.put("conservatorio", conservatorio);
            viewName = "conservatorio";
        } catch (BussinessException ex) {
            model.put("bussinessMessages", ex.getBussinessMessages());
            model.put("backURL", request.getContextPath() + "/index.html");
            viewName = "error";
        }

        return new ModelAndView(viewName, model);
    }

    @RequestMapping({"/conservatorio/readForListarCursos"})
    public ModelAndView readForListarCursos(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> model = new HashMap<String, Object>();
        String viewName;

        try {
            int id;
            try {
                id = Integer.parseInt(request.getParameter("id"));
            } catch (NumberFormatException nfe) {
                throw new BussinessException(new BussinessMessage(null, "Se debe escribir un Id válido"));
            }

            Conservatorio conservatorio = conservatorioDAO.get(id);
            if (conservatorio == null) {
                throw new BussinessException(new BussinessMessage(null, "No existe el conservatorio con id=" + id));
            }
            model.put("formOperation", FormOperation.ListarCursos);
            model.put("conservatorio", conservatorio);
            viewName = "conservatorio";
        } catch (BussinessException ex) {
            model.put("bussinessMessages", ex.getBussinessMessages());
            model.put("backURL", request.getContextPath() + "/index.html");
            viewName = "error";
        }

        return new ModelAndView(viewName, model);
    }

    @RequestMapping({"/conservatorio/insert.html"})
    public ModelAndView insert(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> model = new HashMap<String, Object>();
        String viewName;

        try {
            request.setCharacterEncoding("UTF-8");
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex);
        }

        Conservatorio conservatorio = null;
        try {
            conservatorio = conservatorioDAO.create();

            conservatorio.setNombre(request.getParameter("nombre"));
            conservatorio.setCodProv(request.getParameter("codProv"));
            conservatorio.setCodMun(request.getParameter("codMun"));

            conservatorioDAO.saveOrUpdate(conservatorio);

            viewName = "redirect:/index.html";
        } catch (BussinessException ex) {
            model.put("bussinessMessages", ex.getBussinessMessages());
            if (conservatorio != null) {
                conservatorio.setCodigo(0);
            }
            model.put("conservatorio", conservatorio);
            model.put("formOperation", FormOperation.Insert);
            viewName = "conservatorio";
        }

        return new ModelAndView(viewName, model);
    }

    @RequestMapping({"/conservatorio/update.html"})
    public ModelAndView update(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> model = new HashMap<String, Object>();
        String viewName;
        try {
            request.setCharacterEncoding("UTF-8");
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex);
        }
        Conservatorio conservatorio = null;
        try {
            int id;
            try {
                id = Integer.parseInt(request.getParameter("id"));
            } catch (NumberFormatException nfe) {
                throw new BussinessException(new BussinessMessage(null, "Se debe escribir un Id válido"));
            }
            conservatorio = conservatorioDAO.get(id);
            if (conservatorio == null) {
                throw new BussinessException(new BussinessMessage(null, "Ya no existe el conservatorio."));
            }
            conservatorio.setNombre(request.getParameter("nombre"));
            conservatorio.setCodProv(request.getParameter("codProv"));
            conservatorio.setCodMun(request.getParameter("codMun"));

            conservatorioDAO.saveOrUpdate(conservatorio);

            viewName = "redirect:/index.html";
        } catch (BussinessException ex) {
            model.put("bussinessMessages", ex.getBussinessMessages());
            model.put("conservatorio", conservatorio);
            model.put("formOperation", FormOperation.Update);
            viewName = "conservatorio";
        }

        return new ModelAndView(viewName, model);
    }

    @RequestMapping({"/conservatorio/delete.html"})
    public ModelAndView delete(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> model = new HashMap<String, Object>();
        String viewName;

        Conservatorio conservatorio = null;
        try {
            int id;
            try {
                id = Integer.parseInt(request.getParameter("id"));
            } catch (NumberFormatException nfe) {
                throw new BussinessException(new BussinessMessage(null, "Se debe escribir un Id válido"));
            }
            conservatorio = conservatorioDAO.get(id);
            if (conservatorio == null) {
                throw new BussinessException(new BussinessMessage(null, "Ya no existe el conservatorio a borrar"));
            }

            conservatorioDAO.delete(id);

            viewName = "redirect:/index.html";
        } catch (BussinessException ex) {
            model.put("bussinessMessages", ex.getBussinessMessages());
            model.put("conservatorio", conservatorio);
            model.put("formOperation", FormOperation.Delete);
            viewName = "conservatorio";
        }

        return new ModelAndView(viewName, model);
    }

    @RequestMapping({"/conservatorio/listarcursos.html"})
    public ModelAndView listarCursos(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> model = new HashMap<String, Object>();
        String viewName;

        Conservatorio conservatorio = null;
        try {
            int id;
            try {
                id = Integer.parseInt(request.getParameter("id"));
            } catch (NumberFormatException nfe) {
                throw new BussinessException(new BussinessMessage(null, "Se debe escribir un Id válido"));
            }
            conservatorio = conservatorioDAO.get(id);
            if (conservatorio == null) {
                throw new BussinessException(new BussinessMessage(null, "Ya no existe el conservatorio."));
            }

            List<Curso> cursos = cursoDAO.findCursosById(id);

            model.put("cursos", cursos);
            viewName = "cursosLista";
        } catch (BussinessException ex) {
            model.put("bussinessMessages", ex.getBussinessMessages());
            model.put("conservatorio", conservatorio);
            model.put("formOperation", FormOperation.ListarCursos);
            viewName = "conservatorio";
        }

        return new ModelAndView(viewName, model);
    }
}
