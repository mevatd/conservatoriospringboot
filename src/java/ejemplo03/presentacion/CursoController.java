/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo03.presentacion;

import com.fpmislata.persistencia.dao.BussinessException;
import com.fpmislata.persistencia.dao.BussinessMessage;
import ejemplo03.dominio.Conservatorio;
import ejemplo03.dominio.Curso;
import ejemplo03.persistencia.dao.ConservatorioDAO;
import ejemplo03.persistencia.dao.CursoDAO;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Lorenzo González
 */
@Controller
public class CursoController {
/*
    @Autowired
    private CursoDAO cursoDAO;


    @RequestMapping({"/conservatorio/readForListarCursos"})
    public ModelAndView readForListarCursos(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> model = new HashMap<String, Object>();
        String viewName;

        try {
            int id;
            try {
                id = Integer.parseInt(request.getParameter("id"));
            } catch (NumberFormatException nfe) {
                throw new BussinessException(new BussinessMessage(null, "Se debe escribir un Id válido"));
            }

            //Conservatorio conservatorio = conservatorioDAO.get(id);
           // if (conservatorio == null) {
          //      throw new BussinessException(new BussinessMessage(null, "No existe el conservatorio con id=" + id));
           // }
            model.put("formOperation", FormOperation.ListarCursos);
          //  model.put("conservatorio", conservatorio);
            viewName = "conservatorio";
        } catch (BussinessException ex) {
            model.put("bussinessMessages", ex.getBussinessMessages());
            model.put("backURL", request.getContextPath() + "/index.html");
            viewName = "error";
        }

        return new ModelAndView(viewName, model);
    }

    @RequestMapping({"/conservatorio/listarcursos.html"})
    public ModelAndView listarCursos(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> model = new HashMap<String, Object>();
        String viewName;

        //Conservatorio conservatorio = null;
        try {
            int id;
            try {
                id = Integer.parseInt(request.getParameter("id"));
            } catch (NumberFormatException nfe) {
                throw new BussinessException(new BussinessMessage(null, "Se debe escribir un Id válido"));
            }
           // conservatorio = conservatorioDAO.get(id);
           // if (conservatorio == null) {
           //     throw new BussinessException(new BussinessMessage(null, "Ya no existe el conservatorio."));
           // }

            List<Curso> cursos = cursoDAO.findCursosById(id);

            model.put("cursos", cursos);
            viewName = "cursosLista";
        } catch (BussinessException ex) {
            model.put("bussinessMessages", ex.getBussinessMessages());
           // model.put("conservatorio", conservatorio);
            model.put("formOperation", FormOperation.ListarCursos);
            viewName = "conservatorio";
        }

        return new ModelAndView(viewName, model);
    }*/
}
