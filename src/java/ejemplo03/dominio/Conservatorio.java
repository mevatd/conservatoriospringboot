package ejemplo03.dominio;

import com.fpmislata.persistencia.dao.Caption;
import java.io.Serializable;
import java.util.ArrayList;
import org.hibernate.validator.constraints.NotBlank;

public class Conservatorio implements Serializable {

    private int codigo;
    @NotBlank
    @Caption("Nombre")
    private String nombre;
    @NotBlank
    @Caption("Código Província")
    private String codProv;
    @NotBlank
    @Caption("Código Municipio")
    private String codMun;

    //  private ArrayList<Instrumento> instrumentos;
    public Conservatorio() {
    }

    public Conservatorio(int codigo, String nombre, String codProv, String codMun) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.codProv = codProv;
        this.codMun = codMun;
    }

    public Conservatorio(String nombre, String codProv, String codMun) {
        this.nombre = nombre;
        this.codProv = codProv;
        this.codMun = codMun;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodProv() {
        return codProv;
    }

    public void setCodProv(String codProv) {
        this.codProv = codProv;
    }

    public String getCodMun() {
        return codMun;
    }

    public void setCodMun(String codMun) {
        this.codMun = codMun;
    }

    @Override
    public String toString() {
        return "Conservatorio{" + "codigo=" + codigo + ", nombre=" + nombre + ", codProv=" + codProv + ", codMun=" + codMun + '}';
    }

}
