package ejemplo03.dominio;

import com.fpmislata.persistencia.dao.Caption;
import java.io.Serializable;
import java.util.ArrayList;
import org.hibernate.validator.constraints.NotBlank;

public class Curso implements Serializable {

    private int codigo_con;
    private int codigo_ins;
    private int matriculados;

    //  private ArrayList<Instrumento> instrumentos;
    public Curso(int codigo_con, int codigo_ins, int matriculados) {
        this.codigo_con = codigo_con;
        this.codigo_ins = codigo_ins;
        this.matriculados = matriculados;
    }

    public Curso() {
    }

    public int getCodigo_con() {
        return codigo_con;
    }

    public void setCodigo_con(int codigo_con) {
        this.codigo_con = codigo_con;
    }

    public int getCodigo_ins() {
        return codigo_ins;
    }

    public void setCodigo_ins(int codigo_ins) {
        this.codigo_ins = codigo_ins;
    }

    public int getMatriculados() {
        return matriculados;
    }

    public void setMatriculados(int matriculados) {
        this.matriculados = matriculados;
    }

    @Override
    public String toString() {
        return "Curso{" + "codigo_con=" + codigo_con + ", codigo_ins=" + codigo_ins + ", matriculados=" + matriculados + '}';
    }

}
