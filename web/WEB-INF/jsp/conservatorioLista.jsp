<%@page import="org.springframework.web.util.HtmlUtils"%>
<%@page import="ejemplo03.dominio.Conservatorio"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    List<Conservatorio> conservatorios = (List<Conservatorio>) request.getAttribute("conservatorios");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lista de conservatorios</title>
        <link href="<%=request.getContextPath()%>/css/bootstrap.css" rel="stylesheet">
        <link href="<%=request.getContextPath()%>/css/bootstrap-responsive.css" rel="stylesheet">
        <script type="text/javascript"  src="<%=request.getContextPath()%>/js/jquery-1.9.0.js"></script>
        <script type="text/javascript"  src="<%=request.getContextPath()%>/js/bootstrap.js" ></script>
    </head>
    <body style="background:#FDFDFD">
        <div class="row-fluid">
            <div class="span12">&nbsp;</div>
        </div>
        <div class="row-fluid">
            <div class="offset1  span10">
                <div class="row-fluid">
                    <div class="span12">
                        <a id="btnNuevo" class="btn btn-primary" href="<%=request.getContextPath()%>/conservatorio/newForInsert.html">Nuevo Conservatorio</a>
                    </div>
                </div>
                <div class="row-fluid">


                    <div class="span12">



                        <table class="table table-bordered table-hover table-condensed">
                            <thead>
                                <tr>
                                    <th>Código</th>
                                    <th>Nombre</th>
                                    <th>Cod Prof</th>
                                    <th>Cod Mun</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    for (Conservatorio conservatorio : conservatorios) {
                                %>
                                <tr>
                                    <td><a href="<%=request.getContextPath()%>/conservatorio/readForUpdate.html?id=<%=conservatorio.getCodigo()%>" title="Editar" >#<%=conservatorio.getCodigo()%></a></td>
                                    <td><a href="<%=request.getContextPath()%>/conservatorio/readForListarCursos.html?id=<%=conservatorio.getCodigo()%>" title="Listar Cursos" ><%=HtmlUtils.htmlEscape(conservatorio.getNombre())%></a></td>

                                    <td><%=HtmlUtils.htmlEscape(conservatorio.getCodProv())%></td>
                                    <td><%=HtmlUtils.htmlEscape(conservatorio.getCodMun())%></td>

                                    <td>
                                        <a href="<%=request.getContextPath()%>/conservatorio/readForDelete.html?id=<%=conservatorio.getCodigo()%>" title="Borrar" ><i class="icon-trash"></i></a>
                                    </td>
                                </tr>
                                <%
                                    }
                                %>
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
            <div class="span1"></div>
        </div>
    </body>
</html>