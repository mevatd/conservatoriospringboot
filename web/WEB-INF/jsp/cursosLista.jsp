<%@page import="ejemplo03.dominio.Curso"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.springframework.web.util.HtmlUtils"%>
<%@page import="ejemplo03.dominio.Conservatorio"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%

    List<Curso> cursos = (List<Curso>) request.getAttribute("cursos");

%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Listar Cursos</title>
        <link href="<%=request.getContextPath()%>/css/bootstrap.css" rel="stylesheet">
        <link href="<%=request.getContextPath()%>/css/bootstrap-responsive.css" rel="stylesheet">
        <script type="text/javascript"  src="<%=request.getContextPath()%>/js/jquery-1.9.0.js"></script>
        <script type="text/javascript"  src="<%=request.getContextPath()%>/js/bootstrap.js" ></script>
    </head>
    <body style="background:#FDFDFD">
        <div class="row-fluid">
            <div class="span12">&nbsp;</div>
        </div>
        <div class="row-fluid">
            <div class="offset1  span10">

                <div class="row-fluid">


                    <div class="span12">



                        <table class="table table-bordered table-hover table-condensed">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th># Conservatorio</th>
                                    <th># Instrumento</th>
                                    <th># Matriculados</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    for (Curso curso : cursos) {
                                %>
                                <tr>
                                    <td>#</td>
                                    <td><%=curso.getCodigo_con()%></a></td>

                                    <td><%=curso.getCodigo_ins()%></td>
                                    <td><%=curso.getMatriculados()%></td>

                                    <td></td>
                                </tr>
                                <%
                                    }
                                %>
                            </tbody>
                        </table>

                        <div class="row-fluid">
                            <div class="span12">
                                <a class="btn btn-primary" href="<%=request.getContextPath()%>/index.html" >Inicio</a>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <div class="span1"></div>
        </div>
    </body>
</html>